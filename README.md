# ClassicMacros

经典宏插件 ClassicMacros 的 Bug 修复版。
重构中，当前版本即将废弃，请勿使用。

## 安装

- 下载 [ClassicMacros](https://gitee.com/WinnowPlus/ClassicMacros/repository/archive/master.zip)
- 解压下载文件
- 将文件夹 ClassicMacros-master 重命名为 ClassicMacros
- 将文件夹 ClassicMacros 复制到 Wow-安装目录\Interface\AddOns
- 重启游戏

## Bug 修复记录

### Bug1：宏图标在法力值不足时不会变灰

原因分析：ClassicMacros 试图从 GameTooltip 获取法术消耗，但运用 GameTooltip 的方式错误。原代码中，GameTooltip 从 GameTooltipTemplate 继承而来，却又试图通过 AddFontStrings 创建法术消耗的子框体，是自相矛盾的。

修复方案1：不从 GameTooltipTemplate 继承。[CleverMacro.lua](../master/CleverMacro.lua) Line 982，`CM_ScanTip = CreateFrame("GameTooltip", "CM_ScanTip", nil, "GameTooltipTemplate")` 修改为 `CM_ScanTip = CreateFrame("GameTooltip", "CM_ScanTip")`。

修复方案2：仍从 GameTooltipTemplate 继承，按照 GameTooltipTemplate 的子框体命名规则获取法术消耗的子框体。[CleverMacro.lua](../master/CleverMacro.lua) Line 175，`local _, _, cost = string.find(CM_ScanTip.costFontString:GetText() or "", "^(%d+)")` 修改为 `local _, _, cost = string.find(CM_ScanTipTextLeft2 and CM_ScanTipTextLeft2:GetText() or "", "^(%d+)")`，Line993～996 删除。

### Bug2：mouseover 不支持 pfUI 框体

原因分析：系统原生框体将 unit ID 存放于框体的 unit 属性，而 pfUI 框体存放于 label 属性。

修复方案：在 [Compat.lua](../master/Compat.lua) 注册一个 MouseOverResolver，以从 pfUI 框体获取 label。

```lua
CleverMacro.RegisterMouseOverResolver(function(frame)
    if frame:GetName() and string.find(frame:GetName(), "^pf") and frame.label and frame.id then
        return frame.label .. frame.id
    end
end)
```

### ~~Bug3：宏图标在 pfUI 动作条上不会实时更新~~

原因分析：pfUI 没有正确监听动作条更新事件。

修复方案：在 [Compat.lua](../master/Compat.lua) 注册一个 ActionEventHandler，动作条需要更新时通知 pfUI。

```lua
CleverMacro.RegisterActionEventHandler(function(slot, event)
    if(pfUI and pfUI.bars and pfUI.bars.update) then
        pfUI.bars.update[slot] = true
    end
end)
```
